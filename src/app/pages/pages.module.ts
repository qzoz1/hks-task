import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [LoginComponent, DashboardComponent],
  imports: [CommonModule, ComponentsModule],
})
export class PagesModule {}
