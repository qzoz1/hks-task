import { Component, HostBinding, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from './../../service/auth.service';

@Component({
  selector: 'qz-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @HostBinding('class.hasLoader') isSigningIn: boolean = false;

  simulateLogin: boolean = false;

  constructor(
    private authService: AuthService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onLogin(loginCreds: any) {
    if (this.isSigningIn) {
      return;
    }
    this.isSigningIn = true;
    this.authService.login(loginCreds, this.simulateLogin).subscribe({
      next: () => {
        this.isSigningIn = false;
        this.router.navigate(['/dashboard']);
      },
      error: () => {
        this.isSigningIn = false;
        this.matSnackBar.open('Error while login OR Invalid Creds', '', {
          duration: 3000,
        });
      },
    });
  }

  onSimulate(event: any) {
    this.simulateLogin = event.target.checked;
  }
}
