import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qz-bread-crumbs',
  templateUrl: './bread-crumbs.component.html',
  styleUrls: ['./bread-crumbs.component.scss'],
})
export class BreadCrumbsComponent implements OnInit {
  @Input() breadCrumbItems: string[] = [
    'Production',
    'Dying',
    'Recipes',
    'Cow',
  ];

  constructor() {}

  ngOnInit(): void {}
}
