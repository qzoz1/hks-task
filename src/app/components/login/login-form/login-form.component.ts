import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'qz-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  readonly EMAIL_FIELD_NAME = 'email';
  readonly PASS_FIELD_NAME = 'password';

  @Output() onFormSubmitted: EventEmitter<{
    [key: string]: string;
  }> = new EventEmitter<{
    [key: string]: string;
  }>();

  loginFormGroup!: FormGroup;

  ValidationType = ValidationType;

  hidePassword: boolean = true;

  constructor() {}

  ngOnInit(): void {
    this.loginFormGroup = new FormGroup({
      [this.EMAIL_FIELD_NAME]: new FormControl('', [
        Validators.required,
        Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
      ]),
      [this.PASS_FIELD_NAME]: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {
    this.hidePassword = true;
    if (this.loginFormGroup.valid) {
      this.onFormSubmitted.emit(this.loginFormGroup.value);
    }
  }
}

enum ValidationType {
  REQUIRED = 'required',
  INVALID = 'pattern',
}
