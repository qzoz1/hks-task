import {
  AfterContentInit,
  Component,
  ContentChildren,
  Input,
  OnInit,
  QueryList,
} from '@angular/core';
import { WidgetChildDirective } from 'src/app/directives/widget-child.directive';

@Component({
  selector: 'qz-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss'],
})
export class WidgetComponent implements OnInit, AfterContentInit {
  @Input() widgetHeading!: string;

  @ContentChildren(WidgetChildDirective)
  widgetChildren!: QueryList<WidgetChildDirective>;

  onlyChild!: WidgetChildDirective | undefined;

  constructor() {}

  ngOnInit(): void {}

  ngAfterContentInit(): void {
    if (this.widgetChildren && this.widgetChildren.length) {
      if (this.widgetChildren.length === 1) {
        this.onlyChild = this.widgetChildren.get(0);
      }
    }
  }
}
