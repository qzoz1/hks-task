import {
  Component,
  EventEmitter,
  HostBinding,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'qz-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  @HostBinding('class.isCollapsed')
  isCollapsed: boolean = false;

  @Output('onToggle') onToggleEmitter: EventEmitter<boolean> =
    new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  toggleSideBar() {
    this.isCollapsed = !this.isCollapsed;
    this.onToggleEmitter.emit(this.isCollapsed);
  }
}
