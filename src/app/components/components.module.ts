import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Material Module Imports
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';

import { LoginFormComponent } from './login/login-form/login-form.component';
import { WidgetComponent } from './dashboard/widget/widget.component';
import { HeaderComponent } from './dashboard/header/header.component';
import { FooterComponent } from './dashboard/footer/footer.component';
import { SidebarComponent } from './dashboard/sidebar/sidebar.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { BrandLogoComponent } from './brand-logo/brand-logo.component';
import { BreadCrumbsComponent } from './bread-crumbs/bread-crumbs.component';
import { DirectivesModule } from '../directives/directives.module';
import { IconTextPairComponent } from './icon-text-pair/icon-text-pair.component';
import { NotificationPanelComponent } from './notification-panel/notification-panel.component';
import { OptionsPanelComponent } from './options-panel/options-panel.component';

const materialModules = [
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatSnackBarModule,
  MatTabsModule,
  MatIconModule,
  MatBadgeModule,
];

const loginComponents = [LoginFormComponent];

const dashboardComponents = [
  WidgetComponent,
  HeaderComponent,
  FooterComponent,
  SidebarComponent,
];

const exportedComponents = [
  LoginFormComponent,
  WidgetComponent,
  HeaderComponent,
  FooterComponent,
  SidebarComponent,
];

@NgModule({
  declarations: [
    ...loginComponents,
    ...dashboardComponents,
    SearchBoxComponent,
    BrandLogoComponent,
    BreadCrumbsComponent,
    IconTextPairComponent,
    NotificationPanelComponent,
    OptionsPanelComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    ...materialModules,
  ],
  exports: [...exportedComponents, MatSnackBarModule, DirectivesModule],
})
export class ComponentsModule {}
