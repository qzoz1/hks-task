import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qz-icon-text-pair',
  templateUrl: './icon-text-pair.component.html',
  styleUrls: ['./icon-text-pair.component.scss'],
})
export class IconTextPairComponent implements OnInit {
  @Input() icon!: string;
  @Input() badgeCount!: number;

  constructor() {}

  ngOnInit(): void {}
}
