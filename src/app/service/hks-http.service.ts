import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ERequestMethod } from '../enum/e-request-method';
import { IHttp } from '../interface/i-http';

@Injectable({
  providedIn: 'root',
})
export class HksHttpService {
  private readonly BASE_URL = environment.baseUrl;

  constructor(private http: HttpClient) {}

  hitApi(httpInfo: IHttp): Observable<any> {
    let url = httpInfo.url;
    if (!url.startsWith('/')) {
      url = '/' + url;
    }
    const finalURL = this.BASE_URL + url;

    if (httpInfo.method === ERequestMethod.POST) {
      return this.http.post(finalURL, httpInfo.input ?? {}, {
        responseType: 'json',
      });
    } else {
      return this.http.get(finalURL, {
        responseType: 'json',
      });
    }
  }
}
