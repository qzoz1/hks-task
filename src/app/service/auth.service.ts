import { delay, Observable, of } from 'rxjs';
import { HksHttpService } from './hks-http.service';
import { Injectable } from '@angular/core';
import { SessionCacheService } from './cache/session-cache.service';
import { IHttp } from '../interface/i-http';
import { ERequestMethod } from '../enum/e-request-method';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private sessionCacheService: SessionCacheService,
    private hksHttpService: HksHttpService,
    private router: Router
  ) {}

  isAuthenticated() {
    if (this.sessionCacheService.isSessionActive()) {
      return true;
    }
    return false;
  }

  login(loginCreds: any, isTest: boolean = false): Observable<any> {
    const iHttpReq: IHttp = {
      url: 'login.php',
      method: ERequestMethod.POST,
      input: loginCreds,
    };
    if (!isTest) {
      return new Observable((subscriber) => {
        this.hksHttpService.hitApi(iHttpReq).subscribe({
          next: (response) => {
            this.sessionCacheService.createSession();
            subscriber.next();
          },
          error: (error) => {
            this.sessionCacheService.clearActiveSession();
            subscriber.error(error);
          },
        });
      });
    } else {
      return new Observable((subscriber) => {
        of('hello')
          .pipe(delay(5000))
          .subscribe({
            next: (response) => {
              this.sessionCacheService.createSession();
              subscriber.next();
            },
            error: (error) => {
              this.sessionCacheService.clearActiveSession();
              subscriber.error(error);
            },
          });
      });
    }
  }

  logout() {
    this.sessionCacheService.clearActiveSession();
    this.router.navigate(['/']);
  }
}
