import { Injectable } from '@angular/core';
import { CacheMain } from './cache-main';

@Injectable({
  providedIn: 'root',
})
export class SessionCacheService extends CacheMain {
  readonly ACTIVE_SESSION_KEY = 'ACTIVE_SESSION';
  constructor() {
    super('SESSION');
  }

  isSessionActive() {
    if (this.retrieve(this.ACTIVE_SESSION_KEY) === 'isActive') {
      return true;
    }
    return false;
  }

  createSession() {
    this.store(this.ACTIVE_SESSION_KEY, 'isActive');
  }

  clearActiveSession() {
    this.clearRecord(this.ACTIVE_SESSION_KEY);
  }
}
