export class CacheMain {
  readonly PREFIX;
  constructor(prefix: string) {
    this.PREFIX = prefix;
  }

  store(key: string, value: string) {
    key = this.PREFIX + '|' + key;
    localStorage.setItem(key, value);
  }

  retrieve(key: string) {
    key = this.PREFIX + '|' + key;
    return localStorage.getItem(key);
  }

  clearRecord(key: string) {
    key = this.PREFIX + '|' + key;
    localStorage.removeItem(key);
  }
}
