import { ERequestMethod } from '../enum/e-request-method';

export interface IHttp {
  url: string;
  method: ERequestMethod;
  input?: any;
}
