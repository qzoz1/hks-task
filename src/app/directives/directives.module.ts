import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetChildDirective } from './widget-child.directive';

@NgModule({
  declarations: [WidgetChildDirective],
  imports: [CommonModule],
  exports: [WidgetChildDirective],
})
export class DirectivesModule {}
