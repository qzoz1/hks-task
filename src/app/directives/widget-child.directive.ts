import { Directive, Input, TemplateRef } from '@angular/core';

@Directive({
  selector: '[qzWidgetChild]',
})
export class WidgetChildDirective {
  @Input('sectionLabel') sectionLabel!: string;

  constructor(public templateRef: TemplateRef<any>) {}
}
